import QtQuick 2.3
import PhotoPlugin 1.0
import QtQuick.Window 2.2

Item {
    id: root
    width: parent.width
    height: parent.height



    /******************************
     *    Photo initializtion     *
     ******************************/

    /* Photoplugin initialization */
    Photo {
        id: s_photo
    }

    /* Fontloader initialization */
    FontLoader {
        id: localFont
        source: "font/HelveticaNeueLTStd-ThEx.otf"
    }

    /* Photo frame one for first slide */
    Rectangle {
        id: photoFrame1
        width: root.width
        height: root.height


        Image {
            id: image1
            source: "file:" + s_photo.Photosource
            anchors.fill: parent
        }
        MouseArea {
            id: photoFrame1area
            anchors.fill: parent
        }
    }

    /* Photo frame for second slide */
    Rectangle {
        id: photoFrame2
        width: root.width
        height: root.height
        x: root.width

        Image {
            id: image2
            source: "file:" + s_photo.Nextslide
            anchors.fill: parent
            onSourceChanged: {
                if (animationFrame.running == false) {
                    root.state = "ClosephotoFrame2"
                    root.state = "OpenphotoFrame2"
                    s_photo.AnimationStatus = -1
                }
            }
        }
        MouseArea {
            id: photoFrame2area
            anchors.fill: parent
        }
    }

    /* Photo description */
    Text {
        id:photo_description
        width:root.width/4.44 ; height:root.height/14.29
        x:root.width/15.44 ; y:root.height/1.10

        font.pointSize: root.width/30
        font.family: localFont.name
        font.bold: true
        color: "#FFFFFF"
        text: s_photo.PicLabel
        style: Text.Outline
    }

    /******************************
     * States and Transitions     *
     ******************************/

    states: [
        State {
            name: "OpenphotoFrame2";
            PropertyChanges {
                target: photoFrame1
                x: -root.width
            }
            PropertyChanges {
                target: photoFrame2
                x: 0
            }
        },
        State {
            name: "ClosephotoFrame2";
            PropertyChanges {
                target: photoFrame1
                x: 0
            }
            PropertyChanges {
                target: photoFrame2
                x: root.width
            }
        }
    ]

    transitions: [
        Transition {
            id: transitionRun
            from: "*"
            to: "OpenphotoFrame2"
            ParallelAnimation {
                NumberAnimation {
                    target: photoFrame1
                    properties: "x";
                    duration: 2000;

                    easing.type: Easing.OutQuart

                }
                NumberAnimation {
                    id: animationFrame
                    target: photoFrame2
                    properties: "x";
                    duration: 2000;

                    easing.type: Easing.OutQuart
                }
            }
            onRunningChanged: {
                if (transitionRun.running == false){
                    s_photo.AnimationStatus = 1
                }
            }
        }
    ]
}


import QtQuick 2.3
import QtQuick.Window 2.2
//import "qrc:/"

Window {
    visible: true
    visibility: "FullScreen"

//    width: 400
//    height: 400
    width: Screen.width
    height: Screen.height

    Rectangle {
        id:roots
        width: parent.width
        height: parent.height


        Photoshow {
            id: show
            x:0 ; y:0
            width:roots.width
            height: roots.height
        }

        Image {
            source:"PhotoFrame_images/ebene_2.png"
            id:ebene_2
            x:roots.width/3.47 ; y:-2
            width:roots.width/2.34
            height:roots.height/5.79
        }

    }
}

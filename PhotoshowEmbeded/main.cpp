#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QCursor>
int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    QCursor::setPos(0,0);

    return app.exec();
}

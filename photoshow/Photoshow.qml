import QtQuick 2.3
import PhotoPlugin 1.0
import QtQuick.Window 2.2

Item {
    id: root
    width: parent.width
    height: parent.height

    Photo {
        id: s_photo
    }

    Rectangle {
        id: photoFrame1
        width: root.width
        height: root.height


        Image {
            id: image1
            source: s_photo.Photosource
            anchors.fill: parent
        }
        MouseArea {
            id: photoFrame1area
            anchors.fill: parent
        }
    }

    Rectangle {
        id: photoFrame2
        width: root.width
        height: root.height
        x: root.width

        Image {
            id: image2
            source: s_photo.Nextslide
            anchors.fill: parent
            onSourceChanged: {
                if (animationFrame.running == false) {
                    root.state = "ClosephotoFrame2"
                    root.state = "OpenphotoFrame2"
                    s_photo.AnimationStatus = -1
                }
            }
        }
        MouseArea {
            id: photoFrame2area
            anchors.fill: parent
        }
    }

    FontLoader {
        id: localFont
        source: "font/HelveticaNeueLTStd-ThEx.otf"
    }

    Rectangle {
        id: rectLabel
        width:root.width/4.44 ; height:root.height/14.29
        x:root.width/9.44 ; y:root.height/1.10
        color: "#FFFFFF"

        Text {
            id:lorem_ipsum

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: root.width/40
            font.family: localFont.name
            color: "#000000"
            text: s_photo.PicLabel
        }
        }
    states: [
        State {
            name: "OpenphotoFrame2";
            PropertyChanges {
                target: photoFrame1
                x: -root.width
            }
            PropertyChanges {
                target: photoFrame2
                x: 0
            }
        },
        State {
            name: "ClosephotoFrame2";
            PropertyChanges {
                target: photoFrame1
                x: 0
            }
            PropertyChanges {
                target: photoFrame2
                x: root.width
            }
        }
    ]

    transitions: [
        Transition {
            id: transitionRun
            from: "*"
            to: "OpenphotoFrame2"
            ParallelAnimation {
                NumberAnimation {
                    target: photoFrame1
                    properties: "x";
                    duration: 1000;
                    easing.amplitude: 0.25
                    easing.type: Easing.OutBounce

                }
                NumberAnimation {
                    id: animationFrame
                    target: photoFrame2
                    properties: "x";
                    duration: 1000;
                    easing.amplitude: 0.25
                    easing.type: Easing.OutBounce
                }
            }
            onRunningChanged: {
                if (transitionRun.running == false){
                    s_photo.AnimationStatus = 1
                }
            }
        }
    ]
}


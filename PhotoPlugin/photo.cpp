#include "photo.h"
#include <iostream>

/// ctor
Photo::Photo(QQuickItem *parent):
    QQuickItem(parent)
{
    /* Directory initialization */

    /// Picture initialization
    path.cd("images");
    list = path.entryList(QDir::Files);
    list.sort();

    /* Label initialization */
    labelMapper.insert(list[0],  "");
    labelMapper.insert(list[1],  "Döner Teller");
    labelMapper.insert(list[2],  "Döner Tasche");
    labelMapper.insert(list[3],  "Lahmacun");
    labelMapper.insert(list[4],  "Iskender Kebap");
    labelMapper.insert(list[5],  "Döner Baguette");
    labelMapper.insert(list[6],  "Salat");
    labelMapper.insert(list[7],  "Pizza");
    labelMapper.insert(list[8],  "Döner Dürüm");
    labelMapper.insert(list[9],  "Köfte Teller");
    labelMapper.insert(list[10], "Überbackenes");
    labelMapper.insert(list[11], "Börek");
    labelMapper.insert(list[12], "Baguette");
    labelMapper.insert(list[13], "Golden Pizza");
    labelMapper.insert(list[14], "Anatolien Spezial");
    labelMapper.insert(list[15], "Calzone");
    labelMapper.insert(list[16], "Calzone");
    labelMapper.insert(list[17], "Alles auch zum Mitnehmen");
    labelMapper.insert(list[18], "Hänchen Döner");
    labelMapper.insert(list[19], "Eigen Produktion");

    /* first photo initialization */
    setPhotosource("images/"+list[0]);
    setPicLabel(labelMapper[list[0]]);
    nextSlideCount = 1;
    currentCount = 0;

    setAnimationStatus(-1);

    /* Timer Settings */
    m_timer.setInterval(5000);
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(changePhoto()));
    m_timer.start();
}


/// dtor
Photo::~Photo()
{
}


/// Getter and Setter for Photosource
QString Photo::Photosource()
{
    return m_Photosource;
}

void Photo::setPhotosource(QString Photosource)
{
    if(Photosource != m_Photosource) {
        m_Photosource = Photosource;
    }
}


/// Getter and Setter for Nextslide
QString Photo::Nextslide()
{
    return m_Nextslide;
}

void Photo::setNextslide(QString Nextslide)
{
    if(Nextslide != m_Nextslide) {
        m_Nextslide = Nextslide;
    }
}

/// Getter and Setter for Animationstatus
qint32 Photo::AnimationStatus()
{
    return m_AnimationStatus;
}

void Photo::setAnimationStatus(qint32 AnimationStatus)
{
    if(AnimationStatus != m_AnimationStatus) {
        m_AnimationStatus = AnimationStatus;
    }
}

/// Getter and Setter for Pic label
QString Photo::PicLabel()
{
    return m_PicLabel;
}

void Photo::setPicLabel(QString PicLabel)
{
    if(PicLabel != m_PicLabel) {
        m_PicLabel = PicLabel;
    }
}

/// Public method
void Photo::changePhoto() {
    if (AnimationStatus() == 1) {
        setPhotosource(Nextslide());
        emit PhotosourceChanged();

        setAnimationStatus(-1);
    }
    else if(AnimationStatus() == -1) {
        setNextslide("images/"+list[nextSlideCount]);
        emit NextslideChanged();

        setPicLabel(labelMapper[list[nextSlideCount]]);
        emit PicLabelChanged();


        // back to first picture
        if (nextSlideCount == list.count() - 1) {
            nextSlideCount = 0;
            return;
        }

        nextSlideCount += 1;
    }
}

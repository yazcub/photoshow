#ifndef PHOTOPLUGIN_PLUGIN_H
#define PHOTOPLUGIN_PLUGIN_H

#include <QQmlExtensionPlugin>

class PhotoPluginPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif // PHOTOPLUGIN_PLUGIN_H


#ifndef PHOTO_H
#define PHOTO_H

#include <QQuickItem>
#include <QString>
#include <QTimer>
#include <QDir>
#include <QFile>

class Photo : public QQuickItem
{
    Q_OBJECT
    Q_DISABLE_COPY(Photo)

    Q_PROPERTY(QString Photosource     READ Photosource     WRITE setPhotosource     NOTIFY PhotosourceChanged)
    Q_PROPERTY(QString Nextslide       READ Nextslide       WRITE setNextslide       NOTIFY NextslideChanged)
    Q_PROPERTY(qint32  AnimationStatus READ AnimationStatus WRITE setAnimationStatus NOTIFY AnimationStatusChanged)
    Q_PROPERTY(QString PicLabel        READ PicLabel        WRITE setPicLabel        NOTIFY PicLabelChanged)

public:
    Photo(QQuickItem *parent = 0);
    ~Photo();

    QString Photosource();
    void setPhotosource(QString Photosource);

    QString Nextslide();
    void setNextslide(QString Nextslide);

    qint32 AnimationStatus();
    void setAnimationStatus(qint32 AnimationStatus);

    QString PicLabel();
    void setPicLabel(QString PicLabel);

private:
    QString m_Photosource;
    QString m_Nextslide;
    qint32 m_AnimationStatus;
    int currentCount;
    QString m_PicLabel;


    QDir path;
    QStringList list;
    QMap <QString, QString> labelMapper;

    qint32 nextSlideCount;

    QTimer m_timer;



signals:
    void PhotosourceChanged();
    void NextslideChanged();
    void AnimationStatusChanged();
    void PicLabelChanged();

private slots:
    void changePhoto();
};

#endif // PHOTO_H


#include "photoplugin_plugin.h"
#include "photo.h"

#include <qqml.h>

void PhotoPluginPlugin::registerTypes(const char *uri)
{
    // @uri PhotoPlugin
    qmlRegisterType<Photo>(uri, 1, 0, "Photo");
}


